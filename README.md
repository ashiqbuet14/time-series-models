# Time Series models





## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ashiqbuet14/time-series-models.git
git branch -M main
git push -uf origin main
```
##Rquirements
Python 3.7
PyTorch 1.3.1
torchaudio 0.3.2
torchdiffeq 0.0.1
Sklearn 0.22.1
sktime 0.3.1
tqdm 4.42.1

## Running the code

```

import sepsis
sepsis.run_all(intensity=False, device='cuda')

```
